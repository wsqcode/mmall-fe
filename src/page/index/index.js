/*
* @Author: wsq
* @Email:   wsqcode@163.com
* @Date:   2018-01-12 14:53:30
* @Last Modified by:   wsq
* @Last Modified time: 2018-01-12 14:53:30
*/

// 'use strict';
// require('page/common/nav/index.js');
// require('page/common/header/index.js');
// var navSide         = require('page/common/nav-side/index.js');
// var _mm = require('util/mm.js');

// navSide.init({});

// require('./index.css');
// require('../module.js');

// alert('message...');


/*_mm.request({
    url: '/product/list.do?keyword=1',
    success: function(res){
        console.log(res);
    },
    error: function(errMsg){
        console.log(errMsg);
    }
});*/

// getUrlParam()方法测试
// console.log(_mm.getUrlParam('test'));


//renderHtml()方法测试
/*var html = '<div>{{data}}</div>';
var data = {
    data : 123
}
console.log(_mm.renderHtml(html, data));*/

require('./index.css');
require('page/common/nav/index.js');
require('page/common/header/index.js');
require('util/slider/index.js');
var navSide         = require('page/common/nav-side/index.js');
var templateBanner  = require('./banner.string');
var _mm             = require('util/mm.js');

$(function() {
    // 渲染banner的html
    var bannerHtml  = _mm.renderHtml(templateBanner);
    $('.banner-con').html(bannerHtml);
    // 初始化banner
    var $slider     = $('.banner').unslider({
        dots: true
    });
    // 前一张和后一张操作的事件绑定
    $('.banner-con .banner-arrow').click(function(){
        var forward = $(this).hasClass('prev') ? 'prev' : 'next';
        $slider.data('unslider')[forward]();
    });
});
